# Docker For Morbis Apps Development

Docker Running with Debian , Apache , Php 5, Oracle Database , Oracle Interface.

Support with : 
- Oracle XE 10g
- Instantclient 12
- Php 5.6
- Apache Server
- Linux Debian


# Installation
How to install Apps with docker

## Install Docker

	Download docker in https://docs.docker.com/get-docker/
	Run and install it.

## First Time Use
	1. This Docker must running in Linux Container
	2. Replace your app in to public/ and you can code your project in public/
	3. Replace your database in to database/
	4. Finish and keep read README.md

## Install OracleXE 10g
Aplikasi ini sudah terinstall Oracle XE 10g secara otomatis, silakan akses http://localhost:1003/apex. (Apabila belum bisa akses, silakan tunggu 2 - 5 menit)
Buat User baru di http://localhost:1003/apex
Default root user -> ( system / oracle )

## Import DB
Folder untuk database ada di => /database/
Silakan paste / replace database dmp anda di folder database/ dengan format database.dmp lalu jalankan build docker kembali.

	$ ssh root@localhost -p 10000
	$ Input password : admin
	$ cd /home/database
	$ imp	

	Input username dan password database anda
	Ikuti Langkah - langkah
	Input username ketika export database

## Setup Application DB connection
	1. setting database anda seperti contoh # Sample Database
	2. replace db username dan password
		Username: usernamedb anda
		Password: passworddb anda
	3. replace db hostname 'host.docker.internal' / 'localhost'

## Sample Database

	define('DB_NAME', 'xe'); 
	define('DB_USER', 'database_user');
	define('DB_PASSWORD', 'database_password');
	define('DB_HOST', 'host'); // => If Docker Database use : localhost , If external database use : host.docker.internal , if from ip public use : your ip public
	define('DB_PORT','10002'); // => if Local Docker use 10002 , If external database use 1521 default port.
	define('DB_DRIVER', 'oci8'); 
	define('PAGINATE_LIMIT', 20);

## Build the Docker image
From your docker directory

    cd mainly folder/
    $ docker-compose --compatibility up -d --build

## Default Usage
Start Container
    
	docker-compose  --compatibilityup -d

Stop container with the Apache2 Debian Default Page

    docker-compose  --compatibility down

## Open Port
Port Usage

	:1000 => 22 ( root / admin )
	:80 => 80 / application 
	:1002 => 1521
	:1003 => 8080
	:443  => 443

## Test your deployment :
* Open [http://127.0.0.1](http://127.0.0.1) in your browser for run application
* Open [http://127.0.0.1:1003/apex](http://127.0.0.1:1003/apex) in your browser for run oracle apex

## Run Docker with Instantly [recommended]
Open Run Docker Folder : 

    Run with shell script :
	
	1. build-docker.sh 
	2. start-docker.sh
	3. stop-docker.sh
	4. update-docker.sh
	5. restart-docker.sh

## Your application root in /public/